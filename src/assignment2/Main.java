package assignment2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author samsung
 */
public class Main {
static List <Integer> list1=new ArrayList<Integer>(); 
static int i,value,m;
Random rand = new Random();
static volatile boolean running=true;
    
public void producer() throws InterruptedException
{
    synchronized(this)
    {
       while(running)
       {
        if(list1.size()<20){
        int value=rand.nextInt(30);
        list1.add(value);
        System.out.println("element entered is:"+value);
        this.notify();
        }
        else
        {
        this.wait();
        }
       }
    }
}
    
public synchronized void consumer()
{
   synchronized(this){
   while(running)
   {
    if(list1.size()>0)
    {
         System.out.println("The removed element is:"+list1.remove(0));
           this.notify();
    }
    else
    {
        try {
            this.wait();
        } catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   }
}

}


public static void main(String args[]) throws InterruptedException, IOException
{
final Main main=new Main();
System.out.println("Welcome user,please proceed as per the instructions to maintain the producer and consumer list");
System.out.println("The producer section adds the elements to the list and the removed elements from the list are displayed: ");
{
 Thread a1 = new Thread(new Runnable(){
   public void run(){
       try {
           main.producer();
       } catch (InterruptedException ex) {
           Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
       }
       }} );

 Thread a2 = new Thread(new Runnable(){
   public void run(){
    main.consumer();     
}});
 System.out.println("The program will stop as soon as you enter the Enter key");
 a1.start(); 
 a2.start();
 InputStreamReader obj=new InputStreamReader(System.in);
 BufferedReader obj1=new BufferedReader(obj);
 obj1.readLine();
 running=false;
 a1.join();
 a2.join();
 
 
}   }
}


